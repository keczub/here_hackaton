import asyncio
from aiohttp import ClientSession
import numpy as np
import json
import matplotlib.pyplot as plt

#asyncio articles
#http://skipperkongen.dk/2016/09/09/easy-parallel-http-requests-with-python-and-asyncio/
#https://pawelmhm.github.io/asyncio/python/aiohttp/2016/04/22/asyncio-aiohttp.html
#https://hackernoon.com/asyncio-for-the-working-python-developer-5c468e6e2e8e

async def fetch(url, session):
    """ Asynchronously read respons from URL """
    
    async with session.get(url) as response:
        return await response.read()

async def run(long1, lat1, long2, lat2):
    """ Get request for given coordinates in both way from A->B, B->A """
    
    # Variables, fixed date and APP id, code
    steam = 'https://route.api.here.com/routing/7.2/calculateroute.json?'
    APP_ID = 'app_id=Oc0650Nrp3J5ueEj4zT4'
    APP_CODE = '&app_code=9DDrI3kSrQ0h_jcrG7yasA'
    waypoint0 = '&waypoint0=geo!' + str(long1) + ',' + str(lat1) #52.3235,21.0326 
    waypoint1 = '&waypoint1=geo!' + str(long2) + ',' + str(lat2) #52.2370,20.9809
    mode = '&mode=fastest;car;traffic:enabled;'
    departure_date = '&departure=2018-09-05T'
    
    hours = ['00', '01', '02', '03', '04', '05', '06', '07',
              '08', '09', '10', '11', '12', '13', '14', '15', 
              '16', '17', '18', '19', '20', '21', '22', '23']
    
    urls_AB = []
    urls_BA = []
    
    for i in hours:
    
        urls_AB.append(steam + APP_ID + APP_CODE + waypoint0 +
                       waypoint1 + mode + departure_date +i+':00:00')
        
    waypoint0 = '&waypoint0=geo!' + str(long2) + ',' + str(lat2) #52.2370,20.9809
    waypoint1 = '&waypoint1=geo!' + str(long1) + ',' + str(lat1) #52.3235,21.0326
    
    for i in  hours:
    
        urls_BA.append(steam + APP_ID + APP_CODE + waypoint0 +
                       waypoint1 + mode + departure_date +i+':00:00')

    urls = urls_AB + urls_BA

    tasks = []

    # Fetch all responses within one Client session,
    # keep connection alive for all requests.
    async with ClientSession() as session:
        for i in urls:
            task = asyncio.ensure_future(fetch(i, session))
            tasks.append(task)

        responses = await asyncio.gather(*tasks)
        # you now have all response bodies in this variable
        return (responses)

def calculateHistogram(long1, lat1, long2, lat2):
    """ Function returns two numpy arrays of shape (24, 3)
        Distance, trafficTime, baseTime
        Example input: 52.3235, 21.0326, 52.2370, 20.9809
        Example output: np.array([[15671, 1141, 1141], ...])
        When error occurs arrays of zeros is returned """ 
    
    try:
        loop = asyncio.get_event_loop()
        future = asyncio.ensure_future(run(long1, lat1, long2, lat2))
        wynik = loop.run_until_complete(future)
        
        wynik_macierz = []
        for i in wynik:
            wynik_json = json.loads(i.decode("utf-8"))
            route = wynik_json['response']['route'][0]
            wynik_macierz.append([route['summary']['distance'],
                                  route['summary']['trafficTime'],
                                  route['summary']['baseTime']])


        wynik_macierz = np.array(wynik_macierz)
        
        return wynik_macierz[:24, :], wynik_macierz[24:,:]
    except:
        print("Generating histogram error")
        return np.zeros(24, 3), np.zeros(24, 3)
    
def visualizeHistograms(arrayAB, arrayBA, location):
    """ Generate visualization for given coordinates """
        
    fig, axs = plt.subplots(2, 1)
    plt.tight_layout(4)
    axs[0].bar(list(range(arrayAB.shape[0])),
             (arrayAB[:, 1]-arrayAB[:, 2])*100 / arrayAB[:, 2])
    axs[0].set_title('Route from dest. A to dest. B', fontsize=10)
    axs[0].set_xlabel('Hour (h)')
    axs[0].set_ylabel('Percent of time \n spent in traffic (%)')
    fig.suptitle("Location: {}".format(location), fontsize=12, fontweight='bold')
    
    axs[1].bar(list(range(arrayBA.shape[0])),
             (arrayBA[:, 1]-arrayBA[:, 2])*100 / arrayBA[:, 2])
    axs[1].set_xlabel('Hour (h)')
    axs[1].set_title('Route from dest. B to dest. A', fontsize=10)
    axs[1].set_ylabel('Percent of time \n spent in traffic (%)')
    
    plt.show()
    
def main():
    # Calculation route 
    #A 54.3551, 18.6451 Gdansk Old Town
    #B 54.3970, 18.6070 Random location in Zaspa District
    
    #A 52.2308, 21.0000 Warsaw Center
    #B 52.1532, 21.0275 Random location in Marki District
    
    
    #A 52.5163, 13.3773 Berlin near Brandenburger Tor
    #B 52.4789, 13.4342 Random location in Neukoln District
    wynikAB, wynikBA = calculateHistogram(52.2308, 21.0000, 52.1532, 21.0275)
    visualizeHistograms(wynikAB, wynikBA, "Warsaw")
    
if __name__ == "__main__":
    main()



