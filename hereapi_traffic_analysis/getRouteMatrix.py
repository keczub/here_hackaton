# -*- coding: utf-8 -*-
"""
Created on Sat Sep  1 13:15:25 2018

@author: keczub
"""

import psycopg2
import time
import requests

def getMatrix(start0: [float, float], destination0: [float, float],
              destination1: [float, float], destination2: [float, float]) -> list:
    """ Returns values for requested route matrix from start point 
    to three selected destinations """

    service_str = 'https://matrix.route.api.here.com/routing/7.2/calculatematrix.json?'
    main_str = "app_id={0}&app_code={1}&start0={2}&destination0={3}&destination1={4}&destination2={5}".format(app_id,
                                                                                  app_code,
                                                                                  start0,
                                                                                  destination0,
                                                                                  destination1,
                                                                                  destination2)
    params_str='&mode=fastest;car;traffic:enabled&summaryAttributes=traveltime,distance'

    r = requests.get(service_str + main_str + params_str)
                    
    metaInfo = r.json()['response']['metaInfo']
    matrixEntry = r.json()['response']['matrixEntry']
    
    return([metaInfo['timestamp'], 
           matrixEntry[0]['summary']['distance'], matrixEntry[0]['summary']['travelTime'],
           matrixEntry[1]['summary']['distance'], matrixEntry[1]['summary']['travelTime'], 
           matrixEntry[2]['summary']['distance'], matrixEntry[2]['summary']['travelTime']])
    
def calculateMatrix(conn: psycopg2.extensions.connection) -> list:
    """Perform a matrix route calulations for """
    
    cur = conn.cursor()
    cur.execute("""SELECT id, ST_Y(geom), ST_X(geom) from "miejscaDocelowe" """)
    miejscaDocelowe = cur.fetchall()
    
    cur.execute("""SELECT id, ST_Y(geom), ST_X(geom) from "punktySiatki350" """)
    punktySiatki = cur.fetchall()
    cur.close()

    wynik = []
    for i in punktySiatki:
        start0 = str(i[1]) + "," + str(i[2])
        destination0=str(miejscaDocelowe[0][1])+","+str(miejscaDocelowe[0][2])
        destination1=str(miejscaDocelowe[1][1])+","+str(miejscaDocelowe[1][2])
        destination2=str(miejscaDocelowe[2][1])+","+str(miejscaDocelowe[2][2])
        
        wynik.append(getMatrix(start0, destination0, destination1, destination2))
        
    return wynik

def createTable(conn: psycopg2.extensions.connection, name: str):
    """ Creates table of given name """
    
    cur = conn.cursor()
    cur.execute("""CREATE TABLE public."{0}" \
                             (rowid SERIAL,  \
                             timestr character(40), \
                             d1 integer,  \
                             t1 integer,  \
                             d2 integer,  \
                             t2 integer,  \
                             d3 integer,  \
                             t3 integer) """.format(name))
    conn.commit()
    cur.close()

def fillTable(conn: psycopg2.extensions.connection, wynik: list, name: str):
    """ Fills table with given nested list [[],[],[],[],...] """
    
    for i in wynik:
        timestr = i[0]
        d1, d2, d3 = i[1], i[3], i[5]
        t1, t2, t3 = i[2], i[4], i[6]
        
        cur = conn.cursor()
        cur.execute("""INSERT INTO public."{0}" (timestr,  d1,  t1,  d2,  t2, d3,  t3) VALUES ('{1}', {2}, {3}, {4}, {5}, {6}, {7}) """.format(name, timestr, d1, t1, d2, t2, d3, t3))
        conn.commit()
        cur.close()
        
if __name__ == "__main__":
    app_id = 'Oc0650Nrp3J5ueEj4zT4'
    app_code='9DDrI3kSrQ0h_jcrG7yasA'

    conn = psycopg2.connect("dbname='Here_hackaton' user='postgres' host='localhost' password=''")
    n=0
    while n <100:
            A = time.time()
            # DEFINE TABLE NAME TO COLLECT DATA
            name = "test_20092018_" + str(n)
            try:
                wynik = calculateMatrix(conn)
            except:
                print('Blad generowania macierz')
            
            try:
                createTable(conn, name)
            except:
                print('Blad tworzenia tabeli')
                
            try:
                fillTable(conn, wynik, name)
            except:
                print('Blad wypelniania tabeli')
            
            B=time.time()
            print(name, ": ", B-A)
            n+=1

            if B-A > 1200:
                pass
            else:
                time.sleep(1200 - (B-A))