import classes as cl 
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import random
import numpy as np
import matplotlib.patches as patches
import os.path
import time

def logStateToFile(event):
    
    try:
        wektor = countWektor()
        mission.steps.append(np.concatenate([np.array([int(event.key)]), wektor]))
    except:
        print("Not a numeric key")
        
def countWektor():
    
    try:    
        obecna_wartosc = mission.countPrize(5)
        przeszkoda = mission.checkCollistion()
        wektor = np.concatenate([[mission.countPrize(1)-obecna_wartosc],
                			[mission.countPrize(2)-obecna_wartosc],
                			[mission.countPrize(3)-obecna_wartosc],
                			[mission.countPrize(4)-obecna_wartosc],
                        [mission.countPrize(5)-obecna_wartosc],
                			[mission.countPrize(6)-obecna_wartosc],
                			[mission.countPrize(7)-obecna_wartosc],
                			[mission.countPrize(8)-obecna_wartosc],
                			[mission.countPrize(9)-obecna_wartosc]]).ravel()
        wektor_przeszkoda = np.hstack([wektor, przeszkoda])
        #print(przeszkoda, przeszkoda.shape, wektor, wektor.shape)
        return wektor_przeszkoda
    except Exception as  e:
        print("Unable to calculate wektor: ", e)
        return (np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]))
        
    
def tick(frameNum):
    
    #print(mission.checkCollistion())
    
    if mission.cleared == True:
        plt.close()
    
    if (TRENING == True) and (frameNum%FRAME_RAND == 0):
        mission.samochod.X += random.randint(-2, 2)
        mission.samochod.Y += random.randint(-2, 2)
        
    if (GRA_SIECI == True):
        if (frameNum%FRAME_RAND == 0):
            mission.samochod.X += random.randint(-1, 1)
            mission.samochod.Y += random.randint(-1, 1)
            
        else:
            wektor = countWektor()
            result = loaded_model.predict(wektor.reshape(1, -1))
            result = str(int(result))
            
            if result =='8':
                mission.samochod.Y += 1
                        
            if result =='4':
                mission.samochod.X += -1
                
            if result =='2':
                mission.samochod.Y += -1
                
            if result =='6':
                mission.samochod.X += 1
    
            if result =='9':
                mission.samochod.X += 1
                mission.samochod.Y += 1
                        
            if result =='7':
                mission.samochod.X += -1
                mission.samochod.Y += 1
                
            if result =='1':
                mission.samochod.X += -1
                mission.samochod.Y += -1
                
            if result =='3':
                mission.samochod.X += 1
                mission.samochod.Y += -1
                
            if result =='5':
                mission.samochod.X += 0
                mission.samochod.Y += 0
    
    mission.samochod.tick(frameNum)
    mission.linia.tick(frameNum)
    
    mission.przeszkoda1.tick(frameNum)
    mission.przeszkoda2.tick(frameNum)
    mission.przeszkoda3.tick(frameNum)
    mission.przeszkoda4.tick(frameNum)
    mission.przeszkoda5.tick(frameNum)
    mission.przeszkoda6.tick(frameNum)
    #mission.countPrize()
    #print(int(time.time() - mission.start))
    

    
def main():
    
   anim = animation.FuncAnimation(fig,
                                  tick,
                                  fargs=(), 
                                         interval=50)
   
   # dodanie procedury obsługi zdarzenia wciśnięcia przycisku
   cid_0 = fig.canvas.mpl_connect('key_press_event', logStateToFile)
   cid = fig.canvas.mpl_connect('key_press_event', mission.samochod.buttonPress)
   
   plt.show()
   plt.close()

def saveStates(macierzKrokow, numerMisji, losowosc):
    if TRENING == True:
        np.save("C:\\Users\\keczub\\Documents\\matplotlob_anim\\logs\\macierz"+str(numerMisji)+"_"+str(losowosc)+"_obs.npy",
                np.array(macierzKrokow))
    else:
        pass
      
    
if __name__ == "__main__":
    TRENING = False
    FRAME_RAND = 80
    
    GRA = False
    GRA_SIECI = True
    
    if GRA_SIECI == True or GRA == True:
        from sklearn.externals import joblib
        # load the model from disk
        loaded_model = joblib.load("C:\\Users\\keczub\\Documents\\matplotlob_anim\\NeuralNet\\NN_obs.sav")
        
        fig = plt.figure()
        mission = cl.Mission(fig, 5)
        main()
        saveStates(mission.steps, 5, FRAME_RAND)
        
    elif TRENING == True:
    
        fig = plt.figure()
        mission = cl.Mission(fig, 0)
        main()
        saveStates(mission.steps, 0, FRAME_RAND)
        
        fig = plt.figure()
        mission = cl.Mission(fig, 1)
        main()
        saveStates(mission.steps, 1, FRAME_RAND)
        
        fig = plt.figure()
        mission = cl.Mission(fig, 2)
        main()
        saveStates(mission.steps, 2, FRAME_RAND)
        
        fig = plt.figure()
        mission = cl.Mission(fig, 3)
        main()
        saveStates(mission.steps, 3, FRAME_RAND)
        
        fig = plt.figure()
        mission = cl.Mission(fig, 4)
        main()
        saveStates(mission.steps, 4, FRAME_RAND)

