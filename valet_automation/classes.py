# -*- coding: utf-8 -*-
"""
Created on Sun Oct 21 17:59:13 2018

@author: keczub
"""
import numpy as np
import time
import matplotlib.patches as patches
import matplotlib.pyplot as plt
from scipy.spatial import KDTree
import random

class Mission(object):
    
    def __init__(self, fig, mission):
        # czas rozpoczecia
        self.start = time.time()
        # numer misji [0, 1, 2, 3, 4]
        self.mission = mission
        # status misji
        self.cleared = False
                
        # Inicjalizacja obiektow planszy 
        self.fig = fig
        # Osi w ograniczeniu 120x120
        self.ax = plt.axes(xlim=(0, 120), ylim=(0, 120))
        # Dodanie obiektow poczatku i konca 
        self.addStartMeta()
        
        # Inicjalizacja elementow ruchomych
        self.samochod = Dot(self.ax, self.mission)
        self.linia = Line(self.ax, self.mission)
        
        self.przeszkoda1 = Obstacle(self.ax)
        self.przeszkoda2 = Obstacle(self.ax)
        self.przeszkoda3 = Obstacle(self.ax)
        self.przeszkoda4 = Obstacle(self.ax)
        self.przeszkoda5 = Obstacle(self.ax)
        self.przeszkoda6 = Obstacle(self.ax)

        self.steps = []
        
    def checkCollistion(self):
        
        wsp_przeszkod = np.vstack([np.array([self.przeszkoda1.X, self.przeszkoda1.Y]),
                        np.array([self.przeszkoda2.X, self.przeszkoda2.Y]),
                        np.array([self.przeszkoda3.X, self.przeszkoda3.Y]),
                        np.array([self.przeszkoda4.X, self.przeszkoda4.Y]),
                        np.array([self.przeszkoda5.X, self.przeszkoda5.Y]),
                        np.array([self.przeszkoda6.X, self.przeszkoda6.Y])])
    
        #print(wsp_przeszkod, wsp_przeszkod.shape)
    
        przeszkody = KDTree(wsp_przeszkod)
        
        lista = przeszkody.query_ball_point(np.array([self.samochod.X, self.samochod.Y]), 10)
        if len(lista) == 0:
            return(np.array([0]))
        else:    
            return(np.array([1]))  
        
                 
    def addStartMeta(self):
        
        # Turn off tick labels
        self.ax.set_yticklabels([])
        self.ax.set_xticklabels([])

        if self.mission == 0:
            # Poczatek i koniec 
            rect = patches.Rectangle((108,   0), 10, 10, linewidth=0.5, edgecolor='black', facecolor='green')
            rect1 = patches.Rectangle((60,  6), 10, 10, linewidth=0.5, edgecolor='black', facecolor='yellow')
        elif self.mission == 1:
            rect = patches.Rectangle((38,   108), 10, 10, linewidth=0.5, edgecolor='black', facecolor='green')
            rect1 = patches.Rectangle((98,  37), 10, 10, linewidth=0.5, edgecolor='black', facecolor='yellow')
        elif self.mission == 2:
            rect = patches.Rectangle((1,   109), 10, 10, linewidth=0.5, edgecolor='black', facecolor='green')
            rect1 = patches.Rectangle((61,  18), 10, 10, linewidth=0.5, edgecolor='black', facecolor='yellow')
        elif self.mission == 3:
            rect = patches.Rectangle((22,   -2), 10, 10, linewidth=0.5, edgecolor='black', facecolor='green')
            rect1 = patches.Rectangle((46,  108), 10, 10, linewidth=0.5, edgecolor='black', facecolor='yellow')
        elif self.mission == 4:
            rect = patches.Rectangle((78,  43), 10, 10, linewidth=0.5, edgecolor='black', facecolor='green')
            rect1 = patches.Rectangle((12,  76), 10, 10, linewidth=0.5, edgecolor='black', facecolor='yellow')
        elif self.mission == 5:
            rect = patches.Rectangle((15,  28), 10, 10, linewidth=0.5, edgecolor='black',facecolor='green')
            rect1 = patches.Rectangle((71,  49), 10, 10, linewidth=0.5, edgecolor='black',facecolor='yellow') 
        else:
            pass
        # Add the patch to the Axes
        self.ax.add_patch(rect)
        self.ax.add_patch(rect1)
        
    def countPrize(self, direction): 
        """
        pkt_Samochodu - lista wspolrzednych, [X, Y]
        
        A = np.array([[10, 10],[15, 15],[20, 20]])
        B = np.array([[10, 10]])
        np.apply_along_axis(lambda x: np.linalg.norm(x-B), 1, A)
        returns:
            array([  0.        ,   7.07106781,  14.14213562])
        """
        if direction == 5:
            B = np.array([self.samochod.X, self.samochod.Y])
        elif direction == 1:
            B = np.array([self.samochod.X-1, self.samochod.Y-1])
        elif direction == 2:
            B = np.array([self.samochod.X, self.samochod.Y-1])
        elif direction == 3:
            B = np.array([self.samochod.X+1, self.samochod.Y-1])
        elif direction == 4:
            B = np.array([self.samochod.X-1, self.samochod.Y])
        elif direction == 6:
            B = np.array([self.samochod.X+1, self.samochod.Y])
        elif direction == 7:
            B = np.array([self.samochod.X-1, self.samochod.Y+1])
        elif direction == 8:
            B = np.array([self.samochod.X, self.samochod.Y+1])
        elif direction == 9:
            B = np.array([self.samochod.X+1, self.samochod.Y+1])
        else:
            pass

        #Odleglosc od prostej 
        vec_odl = np.apply_along_axis(lambda x: np.linalg.norm(x-B), 1, self.linia.numeric_trace)
        
        # Pierwotna wersja obliczen roznica pomiedzy X, Y a ostatnim punktem
        vec_pkt = np.linalg.norm(self.linia.numeric_trace[-1]-B)
        
        if (vec_odl[np.argmin(vec_odl)] < 2) and (vec_pkt < 2):
            print("JESTESMY NA MIEJSCU")
            self.endMission()
        else:
            #print("Odl do ostatniego:", vec_pkt)
            #print("Najblizszy:", self.linia.drogaKdTree.query(B))
            vec_pkt2 = self.linia.drogaKdTree.query(B)[0]
            
            if self.linia.drogaKdTree.query(B)[0] < 2:
                #print("Jestes na:", self.linia.drogaKdTree.query(B)[1])
                # Aktualizacja listy
                self.linia.updateLine(self.linia.drogaKdTree.query(B)[1])
                
            return np.array([vec_odl[np.argmin(vec_odl)], vec_pkt2])
        
    def endMission(self):
        end = time.time()
        print("Wynik:", end - self.start)
        self.cleared = True
    
class Dot(object):
    def __init__(self, ax, mission):
        self.mission = mission
        self.X, self.Y  = self.generateCoords()
        self.range = ax.plot([], [], markersize=40, c='none', marker='o', markeredgecolor='b', markeredgewidth=0.25)[0]
        self.pts = ax.plot([], [], markersize=5, c='b', marker='o', ls='None')[0]
        
        
    def generateCoords(self):
        if self.mission == 0 :
            return 113, 1
        elif self.mission == 1 :
            return 41, 112
        elif self.mission == 2 :
            return 6, 114
        elif self.mission == 3 :
            return 27, 3
        elif self.mission == 4 :
            return 83, 48
        elif self.mission == 5 :
            return 20, 31
        else:
            pass
        
    def tick(self, frameNum):
        """aktualizacja symulacji o jeden krok czasowy"""
        # aktualizowanie danych
        self.pts.set_data(self.X, self.Y)
        self.range.set_data(self.X, self.Y)
        
        
    def buttonPress(self, event):
        """procedura obsługi biblioteki matplotlib dla wciskania przycisków"""
        #print(event.key, type(event))
        
        if event.key =='8':
            self.Y += 1
                    
        if event.key =='4':
            self.X += -1
            
        if event.key =='2':
            self.Y += -1
            
        if event.key =='6':
            self.X += 1

        if event.key =='9':
            self.X += 1
            self.Y += 1
                    
        if event.key =='7':
            self.X += -1
            self.Y += 1
            
        if event.key =='1':
            self.X += -1
            self.Y += -1
            
        if event.key =='3':
            self.X += 1
            self.Y += -1
            
        if event.key =='5':
            self.X += 0
            self.Y += 0
            
class Line(object):
    
    def __init__(self, ax, mission):
        self.mission = mission
        self.X, self.Y, self.numeric_trace, self.drogaKdTree = self.trasa()
        self.beak = ax.plot([], [], markersize=0.5, c='black', ls='-.')[0]
        
    def updateLine(self, numer):
        self.numeric_trace = np.delete(self.numeric_trace, numer, 0)
        self.drogaKdTree = KDTree(self.numeric_trace)
    
    def trasa(self):
        
        if self.mission == 0:
            
            droga = np.load("./GeoDane_npy/droga0.npy")
            X = droga[:,0]
            Y = droga[:,1]
            numericTrace = droga
            drogaKdTree = KDTree(numericTrace)

        elif self.mission == 1:
            
            droga = np.load("./GeoDane_npy/droga1.npy")
            X = droga[:,0]
            Y = droga[:,1]
            numericTrace = droga
            drogaKdTree = KDTree(numericTrace)
        
        elif self.mission == 2:
            
            droga = np.load("./GeoDane_npy/droga2.npy")
            X = droga[:,0]
            Y = droga[:,1]
            numericTrace = droga
            drogaKdTree = KDTree(numericTrace)
            
        elif self.mission == 3:
            
            droga = np.load("./GeoDane_npy/droga3.npy")
            X = droga[:,0]
            Y = droga[:,1]
            numericTrace = droga
            drogaKdTree = KDTree(numericTrace)
            
        elif self.mission == 4:
            
            droga = np.load("./GeoDane_npy/droga4.npy")
            X = droga[:,0]
            Y = droga[:,1]
            numericTrace = droga
            drogaKdTree = KDTree(numericTrace)
            
        elif self.mission == 5:
            
            droga = np.load("./GeoDane_npy/droga5.npy")
            X = droga[:,0]
            Y = droga[:,1]
            numericTrace = droga
            drogaKdTree = KDTree(numericTrace)
            
        else:
            pass
        
        return X, Y, numericTrace, drogaKdTree 
        
    def tick(self, frameNum):
        self.beak.set_data(self.X, self.Y)
        
class Obstacle(object):
    
    def __init__(self, ax):
        self.X_sr = random.randint(10, 110)
        self.Y_sr = random.randint(10, 110)
        self.r = random.randint(15, 20)
        self.fi = 0.0
        self.fi_plus = (random.randint(1, 2)/180) * 3.14
        self.obs = ax.plot([], [], markersize=10, c='r', marker='h')[0]
        self.X = self.X_sr + self.r * np.cos(self.fi)
        self.Y = self.Y_sr + self.r * np.sin(self.fi)
        self.direction = random.randint(0, 1)
        
    def move(self):
        if self.direction == 1:
            self.fi += self.fi_plus
        else:
            self.fi -= self.fi_plus
        self.X = self.X_sr + self.r * np.cos(self.fi) #random.randint(-2, 2)
        self.Y = self.Y_sr + self.r * np.sin(self.fi) #random.randint(-2, 2)
        
        if self.X > 120:
            self.X = 119
        elif self.X < 0:
            self.X = 1
            
        if self.Y > 120:
            self.Y == 119
        elif self.Y < 0:
            self.Y = 1
        
    def tick(self, frameNum):
        self.move()
        self.obs.set_data(self.X, self.Y)